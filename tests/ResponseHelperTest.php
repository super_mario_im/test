<?php

use Mariotadic\Cha\Helpers\ResponseHelper;
use PHPUnit\Framework\TestCase;

final class ResponseHelperTest extends TestCase
{
  public function testError(): void
  {
    ResponseHelper::error(400, 'bad input');
    $this->assertEquals(400, http_response_code());
  }

  // other test methods
}
