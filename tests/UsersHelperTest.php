<?php

use Mariotadic\Cha\Helpers\UsersHelper;
use PHPUnit\Framework\TestCase;
use Respect\Validation\Exceptions\ValidatorException;

final class UsersHelperTest extends TestCase
{
  public function testValidateUserDataEmpty(): void
  {
    $this->expectException(ValidatorException::class);

    UsersHelper::validateUserData([]);
  }

  public function testValidateUserDataOnlyName(): void
  {
    $this->expectException(ValidatorException::class);

    UsersHelper::validateUserData(['name' => 'test']);
  }

  public function testValidateUserDataOnlyDate(): void
  {
    $this->expectException(ValidatorException::class);

    UsersHelper::validateUserData(['yearOfBirth' => 1234]);
  }

  public function testValidateUser(): void
  {
    $this->assertNull(UsersHelper::validateUserData(['yearOfBirth' => 1234, 'name' => 'test']));
  }

  // Here other functions from UsersHelper can be tested
}
