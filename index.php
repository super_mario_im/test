<?php
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/db.php';

$router = new \Bramus\Router\Router();
$router->setNamespace('\Mariotadic\Cha\Controllers');

$router->get('/users/(\d+)', 'UserController@getOne');
$router->post('/users/(\d+)', 'UserController@update');
$router->post('/users', 'UserController@create');

$router->set404(function () {
  header('HTTP/1.1 404 Not Found');
  echo "No route";
});

$router->run();
