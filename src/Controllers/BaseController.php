<?php

namespace Mariotadic\Cha\Controllers;

interface BaseController
{
  public static function getOne(int $id): void;
  public static function create(): void;
  public static function update(int $id): void;
}
