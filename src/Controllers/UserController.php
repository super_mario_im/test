<?php

namespace Mariotadic\Cha\Controllers;

use Mariotadic\Cha\Helpers\ResponseHelper;
use Mariotadic\Cha\Helpers\UsersHelper;
use Respect\Validation\Exceptions\ValidatorException;

class UserController implements BaseController
{
  public static function getOne(int $id): void
  {
    try {
      $user = UsersHelper::getUserById($id);
      if (!$user) {
        ResponseHelper::error(404, 'User not found');
      } else {
        ResponseHelper::success("This is {$user['name']}, born in {$user['year_of_birth']}");
      }
    } catch (\Exception $e) {
      ResponseHelper::error(500, 'Something went wrong');
    }
  }

  public static function create(): void
  {
    try {
      UsersHelper::validateUserData($_POST);
      $id = UsersHelper::createNewUser($_POST);
      ResponseHelper::success("New user with id {$id} has been created.");
    } catch (ValidatorException $e) {
      ResponseHelper::error(400, 'Invalid input ' . $e->getFullMessage());
    } catch (\Exception $e) {
      ResponseHelper::error(500, 'Something went wrong');
    }
  }

  public static function update(int $id): void
  {
    try {
      UsersHelper::validateUserData($_POST);

      $user = UsersHelper::getUserById($id);
      if (!$user) {
        ResponseHelper::error(404, 'User not found');
      } else {
        UsersHelper::updateUser($id, $_POST);
        ResponseHelper::success("User has been updated.");
      }
    } catch (ValidatorException $e) {
      ResponseHelper::error(400, 'Invalid input ' . $e->getFullMessage());
    } catch (\Exception $e) {
      ResponseHelper::error(500, 'Something went wrong');
    }
  }
}
