<?php

namespace Mariotadic\Cha\Helpers;

use Respect\Validation\Validator as v;

class UsersHelper
{
  public static function validateUserData(array $data): void
  {
    v::key('name', v::stringType()->notEmpty())
      ->key('yearOfBirth', v::number()->notEmpty())
      ->assert($data);
  }

  public static function getUserById(int $id): array
  {
    $user = \DB::queryFirstRow("SELECT * FROM users WHERE id = %i", $id);

    return $user;
  }

  public static function createNewUser($data): int
  {
    $date = date('Y-m-d');

    \DB::insert('users', [
      'name' => $data['name'],
      'year_of_birth' => $data['yearOfBirth'],
      'created' => $date
    ]);

    return \DB::insertId();
  }

  public static function updateUser(int $id, array $data): bool
  {
    $date = date('Y-m-d');

    return \DB::update('users', [
      'name' => $data['name'],
      'year_of_birth' => $data['yearOfBirth'],
      'updated' => $date,
    ], "id = %i", $id);
  }
}
