<?php

namespace Mariotadic\Cha\Helpers;

/**
 * This methods are just for returning http codes and echoing messages, in real app this would work in some other way
 */
class ResponseHelper
{
  public static function error(int $code, String $message): void
  {
    http_response_code($code);
    echo $message;
  }

  public static function success(String $message): void
  {
    http_response_code(200);
    echo $message;
  }
}
